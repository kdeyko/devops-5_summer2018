#!/bin/bash

ME=`basename $0`

### help func
function print_help() {
    echo
    echo "cool config creator"
    echo
    echo "Usage: $ME -t <file.template> -o <file.conf>"
    echo "Options:"
    echo "  -t <file.template>   specify the template filename"
    echo "  -o <file.conf>       specify the output config filename"
    echo
}


### Print help if no args given
if [ $# -lt 4 ]; then
    print_help
    exit 1
fi

### working with params
while getopts ":t:o:" opt ;
do
    case $opt in
        t) template="$OPTARG";
            ;;
        o) out="$OPTARG";
            ;;
        *) print_help;
           exit 1
            ;;
    esac
done

### check if template exists
if [ ! -f "$template" ]
then
	echo "$template not found!"
	exit 1
fi

### main: parsing template and creating config file
sed -n "s/user_var/"$USER"/p" $template > $out
sed -n "s/host_var/"$HOSTNAME"/p" $template >> $out
sed -n "s!homedir_var!"$HOME"!p" $template >> $out
sed -n "s/locale_var/"$LANG"/p" $template >> $out

echo "DONE!"
echo "config name = $out"
