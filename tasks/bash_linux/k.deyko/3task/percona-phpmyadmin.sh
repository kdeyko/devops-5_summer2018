### installing packages
sudo yum install -y http://www.percona.com/downloads/percona-release/redhat/0.1-4/percona-release-0.1-4.noarch.rpm
sudo yum install -y Percona-Server-server-57
sudo yum install -y httpd
sudo yum install -y php
sudo yum install -y phpmyadmin

### enabling and starting services
sudo systemctl enable mysql
sudo systemctl start mysql
sudo systemctl enable httpd
sudo systemctl start httpd

### configuring firewall
while [[ ! $(sudo firewall-cmd --zone=public --list-services) == *"http"* ]]
do
	((attempt++))
	echo "Configuring firewall rule. Attempt $attempt"
    sudo firewall-cmd --zone=public --add-service=http
done
while [[ ! $(sudo firewall-cmd --zone=public --permanent --list-services) == *"http"* ]]
do
	((attempt_perm++))
	echo "Configuring permanent firewall rule. Attempt $attempt_perm"
    sudo firewall-cmd --zone=public --permanent --add-service=http
done

sudo firewall-cmd --reload

### allow remote connections
sudo sed -i.bak 's/Require ip 127\.0\.0\.1/Require all granted/' /etc/httpd/conf.d/phpMyAdmin.conf

### applying new root password
new_pw="C00lEp@mPassword"
old_pw=$(sudo grep "temporary password" /var/log/mysqld.log | awk '{print $NF}')
mysqladmin -u root -p"$old_pw" password "$new_pw" 2>/dev/null

### restart httpd
sudo systemctl restart httpd