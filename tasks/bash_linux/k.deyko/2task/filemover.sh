#!/bin/bash

function dest_dir_preparing {

	if [ ! -d  "$dest_dir" ]
	then
		echo "Directory $dest_dir not found!"
		echo -n "Trying to create the directory..."
		mkdir "$dest_dir" 2>/dev/null
		if (( $? != 0 ))
		then
			echo
			echo "Cannot create folder, possibly permissions issue."
			echo "Try to specify another directory in $cfgFile or use sudo"
			exit 1
		else
			echo "ok!"
		fi
	else
		if [ ! -w "$dest_dir" ]
		then
			echo "Can't write to this directory: permission denied."
			echo "Try to specify another directory in $cfgFile or use sudo"
			exit 1
		fi
	fi
}

function main {

	files=("$source_dir"/*)
	for f in "${files[@]}"
	do
		f_name=$(basename "$f")
		if [[ ! "$f_name" == "+++"* ]]
		then
			uuid=$(tail -1 "$f" | sed -n '/.\{8\}-.\{4\}-.\{4\}-.\{4\}-.\{12\}/ p')
			if [ ! -z "$uuid" ]
			then
				### if all checks done rename file and preparing to moving
				temp_f="$source_dir/+++$f_name"
				mv "$f" "$temp_f"
				obj_name=$(head -1 "$temp_f" | awk '{print $1}')
				obj_date=$(head -1 "$temp_f" | awk '{print $2}')
				obj_get_date=$(stat -c %y "$temp_f" | awk '{print $1}')
				
				### moving file to dest_dir with renaming
				new_f="$dest_dir/${obj_name}_${obj_date}_${obj_get_date}"
				mv "$temp_f" "$new_f"
				
				### add record to log.txt
				printf "%-20s obj %-20s file  %-11s moved from  %-10s  to  %-10s\n" "$(date "+%Y-%m-%d %H:%M:%S")" "$obj_name" "$f_name" "$source_dir" "$dest_dir" >> "$dest_dir/log.txt"
			fi
		fi
	done
	
	echo "DONE!"
	echo "check $dest_dir/log.txt for detailes"
	echo
}
function archiving {
	
	echo "Archiving..."
	files=($(ls -1drt "$dest_dir"/*))
	for f in "${files[@]}"
	do
		f_name=$(basename "$f")
		if [[ ! "$f_name" == *".txt" ]] && [[ ! "$f_name" == *".tar" ]]
		then
			if_older=$(find "$f" -mtime +$arch_days)
			if [[ "$f" == "$if_older" ]]
			then
				obj_name=$(head -1 "$f" | awk '{print $1}')
				obj_get_date=$(stat -c %y "$f" | awk '{print $1}')
				cur_date=$(date "+%Y-%m-%d")
				tar_name="$dest_dir"/"$obj_name"_"$obj_get_date"_"$cur_date".tar
				existing_tar=$(find "$dest_dir/$obj_name"*.tar 2>/dev/null)
				
				if [[ ! "$existing_tar" == "$tar_name" ]]
				then
					echo "Creating new archive: $tar_name"
					tar cf "$tar_name" -C "$dest_dir" "$f_name" --remove-files
				else
					echo "Adding file to existing archive: $existing_tar "
					tar rf "$existing_tar" -C "$dest_dir" "$f_name" --remove-files
					new_tar_name="${existing_tar::-14}$cur_date.tar"
					if [[ ! "$new_tar_name" == "$existing_tar" ]]
					then
						echo "Renaming archive according to current date: $new_tar_name"
						mv "$existing_tar" "$new_tar_name"
					fi
				fi
			else
				break
			fi
		fi
	done

}

### reporting according to log.txt in $dest_dir
function reporting {

	if [ -z "$rep_start_date" ]
	then
		echo "Please specify the report start date in $cfgFile"
	elif [ -z "$rep_stop_date" ]
	then
		echo "Please specify the report stop date in $cfgFile"
	elif [ ! -e "$dest_dir"/log.txt ]
	then 
		echo "File log.txt not found"
	elif [ -z "$dest_dir"/log.txt ]
	then 
		echo "File log.txt is empty"
	else
		rep_start=$(date --date="$rep_start_date" +%s)
		rep_stop=$(date --date="$rep_stop_date" +%s)
		report_file="$dest_dir"/report.txt
		echo "----------------"
		echo "REPORT for files moved from $rep_start_date to $rep_stop_date" > "$report_file"
		printf "\n%-25s | %-20s\n\n" "Object name" "Files count" >> "$report_file"
		declare -A obj
		
		local IFS=$'\n'
		logfile=$(cat "$dest_dir"/log.txt)
		for line in $logfile
		do
			obj_get_date_human=$(echo $line | awk '/obj/ {print $1, $2}')
			obj_get_date=$(date --date="$obj_get_date_human" +%s)
			if [ $obj_get_date -ge $rep_start ] && [ $obj_get_date -le $rep_stop ]
				then
					obj_name=$(echo $line | awk '/obj/ {print $4}')
					(( obj["$obj_name"]++ ))
			fi
		done
		
		for key in "${!obj[@]}"
		do
			printf "%-25s | %-20s\n" "$key" "${obj[$key]}" >> "$report_file"
		done
		
		echo "Here is your report, it's also saved in $dest_dir as report.txt"
		echo
		cat "$report_file"
	fi

}

: << 'END'
### reporting according to files in $dest_dir
function reporting {

	if [ -z "$rep_start_date" ]
	then
		echo "Please specify the report start date in $cfgFile"
	elif [ -z "$rep_stop_date" ]
	then
		echo "Please specify the report stop date in $cfgFile"
	else
		rep_start=$(date --date="$rep_start_date" +%s)
		rep_stop=$(date --date="$rep_stop_date" +%s)
		report_file="$dest_dir"/report.txt
		echo "----------------"
		echo "REPORT for files moved from $rep_start_date to $rep_stop_date" > "$report_file"
		echo >> "$report_file"
		printf "%-30s | %-30s" "Object name" "Files count" >> "$report_file"
		echo >> "$report_file"
		echo >> "$report_file"
		declare -A obj
		
		files=("$dest_dir"/*)
		for f in "${files[@]}"
		do
			f_name=$(basename "$f")
			if [[ ! "$f_name" == *".txt" ]] && [[ ! "$f_name" == *".tar" ]]
			then
				obj_get_date=$(stat -c %Y "$f")
				if [ $obj_get_date -ge $rep_start ] && [ $obj_get_date -le $rep_stop ]
				then
					obj_name=$(head -1 "$f" | awk '{print $1}')
					(( obj["$obj_name"]++ ))
				fi
				
			fi
		done
		
		for key in "${!obj[@]}"
		do
			printf "%-30s | %-30s" "$key" "${obj[$key]}" >> "$report_file"
			echo >> "$report_file"
		done
		
		echo "Here is your report, it's also saved in $dest_dir as report.txt"
		echo
		cat "$report_file"
	fi
}
END

cfgFile="filemover.cfg"
. $cfgFile

### checking stuff...
if [ -z "$source_dir" ]
then
	echo "Please specify the source dirertory in $cfgFile"
	exit 1
elif [ -z "$dest_dir" ]
then
	echo "Please specify the destination dirertory in $cfgFile"
	exit 1
elif [ ! -d "$source_dir" ]
then
	echo "Directory $source_dir not found!"
	echo "Please check the $cfgFile"
	exit 1
elif [ ! "$(ls -A $source_dir)" ]
then
	echo "Source directory is empty! Nothing to move."
	if $archive
	then
		echo "Going to archiving part of the script..."
		dest_dir_preparing
		if [ ! "$(ls -A $dest_dir_dir)" ]
		then
			echo "Destination directory is empty. Nothing to archive"
		else
			archiving
		fi
	fi
	if $report
	then
		echo "Going to reporting part of the script..."
		dest_dir_preparing
		if [ ! "$(ls -A $dest_dir_dir)" ]
		then
			echo "Destination directory is empty. Nothing to report about"
		else
			reporting
		fi
	fi
else
	dest_dir_preparing
	main
	if $archive
	then
		archiving
	fi
	if $report
	then
		echo "Going to reporting part..."
		reporting
	fi
fi
